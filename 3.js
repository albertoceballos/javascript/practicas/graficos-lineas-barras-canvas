window.addEventListener("load", function(){
	canvas=document.querySelector("canvas");
	ctx=canvas.getContext("2d");

	//datos:
	var datos=new Array();
	datos[0]={};
	datos[0].nombre="Alberto";
	datos[0].valor=10;
	datos[1]={};
	datos[1].nombre="Juan";
	datos[1].valor=30;
	datos[2]={};
	datos[2].nombre="Laura";
	datos[2].valor=25;
	datos[3]={};
	datos[3].nombre="Beatriz";
	datos[3].valor=33;
	datos[4]={};
	datos[4].nombre="Miguel";
	datos[4].valor=42;
	datos[5]={};
	datos[5].nombre="Eva";
	datos[5].valor=28;
	datos[6]={};
	datos[6].nombre="Óscar";
	datos[6].valor=47;
	datos[7]={};
	datos[7].nombre="Ana";
	datos[7].valor=15;
	datos[8]={};
	datos[8].nombre="Vicente";
	datos[8].valor=50;



	//dibujar líneas horizontales mediante un for:
	ctx.beginPath();
	var y=550;
	for(var i=0;i<11;i++){
		ctx.moveTo(0,y);
		ctx.lineTo(800,y);
		y=y-50;
	}
	ctx.stroke();


	//dibujar barras:
	var x=20;
	ctx.beginPath();
	for(i=0;i<datos.length-1;i++){
		ctx.moveTo(x,600-(datos[i].valor*50)/5);
		ctx.arc(x,600-(datos[i].valor*50)/5,10,0,2*Math.PI);
		ctx.fill();
		ctx.beginPath();
		ctx.moveTo(x,600-(datos[i].valor*50)/5);
		ctx.lineTo(x+85,600-(datos[i+1].valor*50)/5);
		ctx.stroke();
		x=x+85;
	}
	ctx.beginPath();
	ctx.arc(x,600-(datos[datos.length-1].valor*50)/5,10,0,2*Math.PI);
	ctx.fill();

	//Escribir nombres de personas:
	var nombres=document.querySelector(".nombres");
	for(i=0;i<datos.length;i++){
		nombres.innerHTML+='<li class="liNombres">'+datos[i].nombre+'</li>';
	}

	//Escribir números:
	for(i=55;i>=0;i-=5){
		document.querySelector(".numeros").innerHTML+='<li class="liNumeros">'+i+'</li>';
	}
});