window.addEventListener("load", function(){
	canvas=document.querySelector("canvas");
	ctx=canvas.getContext("2d");

	//datos:
	var datos=new Array();
	datos[0]={};
	datos[0].nombre="Alberto";
	datos[0].valor=10;
	datos[1]={};
	datos[1].nombre="Juan";
	datos[1].valor=30;
	datos[2]={};
	datos[2].nombre="Laura";
	datos[2].valor=25;
	datos[3]={};
	datos[3].nombre="Beatriz";
	datos[3].valor=33;
	datos[4]={};
	datos[4].nombre="Miguel";
	datos[4].valor=42;
	datos[5]={};
	datos[5].nombre="Eva";
	datos[5].valor=28;
	datos[6]={};
	datos[6].nombre="Óscar";
	datos[6].valor=47;
	datos[7]={};
	datos[7].nombre="Ana";
	datos[7].valor=15;


	//dibujar líneas horizontales mediante un for:
	ctx.beginPath();
	var y=50;
	for(var i=0;i<14;i++){
		ctx.moveTo(y,600);
		ctx.lineTo(y,0);
		y=y+50;
	}
	ctx.stroke();


	//dibujar barras:
	var x=600;
	ctx.beginPath();
	for(i=0;i<datos.length;i++){
		ctx.moveTo(0,x);
		ctx.lineTo((datos[i].valor*50)/5,x);
		ctx.lineTo((datos[i].valor*50)/5,x-30);
		ctx.lineTo(0,x-30);
		ctx.fillStyle="tomato";
		ctx.fill();
		x-=80;
	}

	//Escribir nombres de personas:
	var nombres=document.querySelector(".nombres");
	datos.reverse();
	for(i=0;i<datos.length;i++){
		nombres.innerHTML+='<li class="liNombres">'+datos[i].nombre+'</li>';
	}

	//Escribir números:
	for(i=0;i<60;i+=5){
		document.querySelector(".numeros").innerHTML+='<li class="liNumeros">'+i+'</li>';
	}
});